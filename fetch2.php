<?php

require "./parse_playlist.php";
require "./get_database_ids.php";
require "./insert_spins.php";
require "src/array_column.php";

error_reporting(E_ALL);

// Handle command-line options
$url_longopt                       = "url";
$song_xpath_query_longopt          = "song-xpath-query";
$artist_xpath_query_longopt        = "artist-xpath-query";
$time_xpath_query_longopt          = "time-xpath-query";
$radio_station_xpath_query_longopt = "radio-station-xpath-query";
$time_regex_longopt                = "time-regex";
$radio_station_regex_longopt       = "radio-station-regex";
$radio_station_longopt             = "radio-station";
$help_longopt                      = "help";

$url_shortopt                       = "u";
$song_xpath_query_shortopt          = "s";
$artist_xpath_query_shortopt        = "a";
$time_xpath_query_shortopt          = "t";
$time_regex_shortopt                = "T";
$radio_station_xpath_query_shortopt = "r";
$radio_station_regex_shortopt       = "S";
$radio_station_shortopt             = "R";
$help_shortopt                      = "h";

$shortopts = "u:s:a:t:T:r:S:R:h::";

$longopts = array(
  "$song_xpath_query_longopt:",
  "$artist_xpath_query_longopt:",
  "$time_xpath_query_longopt:",
  "$radio_station_xpath_query_longopt::",
  "$time_regex_longopt::",
  "$radio_station_regex_longopt::",

  "$url_longopt:",
  "$radio_station_longopt:",
  "$help_longopt::",
);

$options = getopt($shortopts, $longopts);

if (isset($options["help"]) or isset($options["h"])) {
  print "
    Usage: $argv[0]
           -$song_xpath_query_shortopt          |--$song_xpath_query_longopt
           -$artist_xpath_query_shortopt        |--$artist_xpath_query_longopt
           -$time_xpath_query_shortopt          |--$time_xpath_query_longopt
           -$time_regex_shortopt                |--$time_regex_longopt
           -$radio_station_xpath_query_shortopt |--$radio_station_xpath_query_longopt
           -$radio_station_regex_shortopt       |--$radio_station_regex_longopt
           -$url_shortopt                       |--$url_longopt
           -$radio_station_shortopt             |--$radio_station_longopt

  ";
  exit();
}

// DEBUGGING
// print '\n options = ' . var_export($options, true);

$spin_tuples = array();

$spin_tuples = parse_playlist(
  $options[$url_longopt]                       ? $options[$url_longopt]                       : $options[$url_shortopt],
  $options[$song_xpath_query_longopt]          ? $options[$song_xpath_query_longopt]          : $options[$song_xpath_query_shortopt],
  $options[$artist_xpath_query_longopt]        ? $options[$artist_xpath_query_longopt]        : $options[$artist_xpath_query_shortopt],
  $options[$time_xpath_query_longopt]          ? $options[$time_xpath_query_longopt]          : $options[$time_xpath_query_shortopt],
  $options[$radio_station_xpath_query_longopt] ? $options[$radio_station_xpath_query_longopt] : $options[$radio_station_xpath_query_shortopt],

  $options[$time_regex_longopt]                ? $options[$time_regex_longopt]                : $options[$time_regex_shortopt],
  $options[$radio_station_regex_longopt]       ? $options[$radio_station_regex_longopt]       : $options[$radio_station_regex_shortopt]
);

// Read in our super-secret (and never-versioned) password file
$dont_you_dare = trim(file_get_contents("./dont_you_dare.txt"));

// Get a database connection
$con = mysqli_connect(
 "radioplaylists.db.10109652.hostedresource.com",
 "radioplaylists",
 "$dont_you_dare",
 "radioplaylists",
 "3306");

if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  exit();
}

$database_ids{"tracks"}         = get_database_ids(array_column($spin_tuples       , 0)      , "tracks");
$database_ids{"artists"}        = get_database_ids(array_column($spin_tuples       , 1)      , "artists");
$database_ids{"origin_url"}     = get_database_ids(array($options{"url"}           => null) , "origin_url");
$database_ids{"radio_stations"} = get_database_ids(array_column($spin_tuples       , 3), "radio_stations");

insert_spins($spin_tuples, $database_ids);

mysqli_close($con);

?>

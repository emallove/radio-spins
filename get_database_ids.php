<?php

// Insert names into id-name table, and return the (id,name) tuples
function get_database_ids($arr, $table_name) {

  global $con;

  $ret = array();

  $array_unique = array_unique($arr);

  $sql = "INSERT INTO $table_name " .
    " (name) VALUES " . join(", ", array_map("wrap_in_squotes_and_parens", $array_unique)) .
    // Ignore duplicate constraint violation since we at least want to INSERT the new names
    " ON DUPLICATE KEY UPDATE dummy=NULL ";

  if (! mysqli_query($con, $sql)) {
    error_log("This query failed: $sql");
    error_log("mysqli_errno: (" . mysqli_errno($con) . ") ");
    error_log("mysqli_error: (" . mysqli_error($con) . ") ");
  }

  $sql = "SELECT id$table_name,name from $table_name WHERE  " .
          " name IN (" . join(", ", array_map("wrap_in_squotes", $array_unique)) . ")";
  
  if (! $result = mysqli_query($con, $sql)) {
    error_log("This query failed: $sql");
    error_log("mysqli_errno: (" . mysqli_errno($con) . ") ");
    error_log("mysqli_error: (" . mysqli_error($con) . ") ");
  }

  while ($row = mysqli_fetch_array($result)) {
    $ret[$row{"name"}] = $row{"id$table_name"};
  }

  // DEBUGGING
  print "\n64 ret = " . var_export($ret, true) . "\n";

  return $ret;
}

function wrap_in_squotes_and_parens($v) {
  global $con;
  return "('" . mysqli_real_escape_string($con, $v) . "')";
}

function wrap_in_squotes($v) {
  global $con;
  return "'" . mysqli_real_escape_string($con, $v) . "'";
}

?>

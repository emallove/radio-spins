#!/bin/sh 

# Ew.
cd /home/content/52/10109652/html/spins

set -x

fetch_path="/var/chroot/home/content/52/10109652/html/spins/fetch2.php"
php_path="/usr/local/bin/php"


# FIXME: this seems like a better bet for spin timestamps
#   //div[@class='row content']//span[@class='timestamp']/@data-date

$php_path -a $fetch_path \
    --radio-station="WERS" \
    --url="http://wers.tunegenie.com" \
    --song-xpath-query="//div[@class = 'row content']//div[@class  = 'song']" \
    --artist-xpath-query="//div[@class  = 'row content']//div[@class  = 'song']/following-sibling::*[1]" \
    --time-xpath-query="//div[@class = 'row content']//span[@class = 'timestamp']" \
    --radio-station-xpath-query="(//section[@id = 'main']//li/a[@class = 'pjax'])[1]"

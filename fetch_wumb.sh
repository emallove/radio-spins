#!/bin/sh 

# Ew.
cd /home/content/52/10109652/html/spins

set -x

fetch_path="/var/chroot/home/content/52/10109652/html/spins/fetch2.php"
php_path="/usr/local/bin/php"


# <table border="0" cellpadding="0" width="100%">
# <tr>
# <td align="left" valign="center" rowspan="2" width="10%" bgcolor="#CCCCCC">
# <b><font face="Arial,Helvetica"><font color="#003366"><font size="-1">
# 9:47 pm</font></font></font></b></td>
# <td colspan="2" bgcolor="#B1C027">
# <b><font face="Arial,Helvetica"><font color="#000000">
# Doc Watson</font></font></b></td></tr>
# <tr>
# <td colspan="2" bgcolor="#0077BE">
# <i><font face="Arial,Helvetica"><font color="#FFFFFF"><font size="+1">
# Way Downtown (from Will The Circle Be Unbroken)</font></font></font></i></td></tr>
# </table>

$php_path -a $fetch_path \
    --radio-station="WUMB" \
    --url="http://wumb.org/cgi-bin/playlist1.pl" \
    --song-xpath-query="//table//tr[position() = 2]//font/font[@color='#FFFFFF']" \
    --artist-xpath-query="//table//tr[position() = 1]//font/font[@color='#000000']" \
    --time-xpath-query="//table//tr[position() = 1]//font/font/font" \
    --radio-station-xpath-query="//title" \
    --radio-station-regex="(\w+)"

<?php

function insert_spins($spin_tuples, $database_ids) {

  global $origin_url, $con;
  $origin_url_id    = $database_ids{"origin_url"}{$origin_url};
  $radio_station_id = $database_ids{"radio_stations"}{$radio_station};

  $rows_inserted = 0;

  for ($i = 0; $i < count($spin_tuples); $i++) {

    $track     = $spin_tuples[$i][0];
    $artist    = $spin_tuples[$i][1];
    $time      = $spin_tuples[$i][2];
    $radio_station = $spin_tuples[$i][3];

    $track_id  = $database_ids["tracks"][$track];
    $artist_id = $database_ids{"artists"}{$artist};
    $radio_station_id = $database_ids{"radio_stations"}{$radio_station};

    $sql = "INSERT INTO spins " .
        " (track_id,
           artist_id,
           time_spun,
           radio_station_id,
           origin_url_id) 
        VALUES 
          ('$track_id',
           '$artist_id',
           '$time',
           '$radio_station_id',
           '$origin_url_id' )";

    if (! mysqli_query($con, $sql)) {
      error_log("This query failed: $sql");
      error_log("mysqli_errno: (" . mysqli_errno($con) . ") ");
      error_log("mysqli_error: (" . mysqli_error($con) . ") ");
    } else {
      $rows_inserted++;
    }
  }

  print("\n$rows_inserted rows inserted\n");
}

?>

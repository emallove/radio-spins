#!/bin/sh 

# Ew.
cd /home/content/52/10109652/html/spins

set -x

fetch_path="/var/chroot/home/content/52/10109652/html/spins/fetch2.php"
php_path="/usr/local/bin/php"

# <strong>St. Croix</strong> <em>Family of the Year</em>
# <hr class="clearfix">
# </a><a href="../radio/playlist.php?station=wbrs&amp;plid=15534&amp;ptype=n#363181" class="spin ev">
# <div class="spec">3:40pm EST<br>WBRS 100.1 FM Waltham</div>


# FIXME: this seems like a better bet for spin timestamps
#   //div[@class='row content']//span[@class='timestamp']/@data-date

while true;
do
  $php_path -a $fetch_path \
      --radio-station="WERS" \
      --url="http://www.spinitron.com" \
      --song-xpath-query="//em" \
      --artist-xpath-query="//strong" \
      --time-xpath-query="//div[@class='spec']" \
      --radio-station-xpath-query="//div[@class='spec']" \
      --time-regex="(.*[ap]m\s+[A-Z]{3})" \
      --radio-station-regex=".*[ap]m\s+[A-Z]{3}(.*)"

  echo "Sleeping for 240 seconds now..."
  sleep 240
done

# This is what we *really* want to do, but loadHTMLFile strips out
# our dear <br> separator
#   --time-regex="(.*)<br>" \
#   --radio-station-regex="<br>(.*)"

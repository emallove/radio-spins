<?php

$dont_you_dare = trim(file_get_contents("./dont_you_dare.txt"));

error_reporting(E_ALL);

// Pass in a WERS URL for an archive playlist (going back ~4 weeks) or not
$origin_url = $argv[1];

if (! $origin_url) {
  $origin_url = "http://wers.tunegenie.com";
}

$doc = new DOMDocument();

// Database connection
$con = mysqli_connect("radioplaylists.db.10109652.hostedresource.com", "radioplaylists", "$dont_you_dare", "radioplaylists", "3306");


// Check connection
if (mysqli_connect_errno()) {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  exit();
}

function wrap_in_squotes_and_parens($v) {
  global $con;
  return "('" . mysqli_real_escape_string($con, $v) . "')";
}

function wrap_in_squotes($v) {
  global $con;
  return "'" . mysqli_real_escape_string($con, $v) . "'";
}


// Insert names into id-name table, and return the (id,name) tuples
function cache_database_ids($arr, $table_name) {

  global $con;

  $sql = "INSERT INTO $table_name " .
    " (name) VALUES " . join(", ", array_map("wrap_in_squotes_and_parens", array_keys($arr))) .
    // Ignore duplicate constraint violation since we at least want to INSERT the new names
    " ON DUPLICATE KEY UPDATE dummy=NULL ";

  if (! mysqli_query($con, $sql)) {
    error_log("This query failed: $sql");
    error_log("mysqli_errno: (" . mysqli_errno($con) . ") ");
    error_log("mysqli_error: (" . mysqli_error($con) . ") ");
    // die();
  }

  $sql = "SELECT id$table_name,name from $table_name WHERE  " .
          " name IN (" . join(", ", array_map("wrap_in_squotes", array_keys($arr))) . ")";
  
  if (! $result = mysqli_query($con, $sql)) {
    error_log("This query failed: $sql");
    error_log("mysqli_errno: (" . mysqli_errno($con) . ") ");
    error_log("mysqli_error: (" . mysqli_error($con) . ") ");
    // die();
  }

  while ($row = mysqli_fetch_array($result)) {
    $arr[$row{"name"}] = $row{"id$table_name"};
  }

  // DEBUGGING
  // print "\n64 arr = " . var_export($arr, true) . "\n";

  return $arr;
}


// In case we get somthing like this:
// Drunk Ibé�??feat. Jay Z
new DOMDocument('1.0', 'utf-8');

$doc->strictErrorChecking = FALSE;


$doc->loadXml('<?xml version="1.0" encoding="utf-8"?><root/>');
$doc = DOMDocument::loadHTMLFile($origin_url);

$xpath = new DOMXPath($doc);

// We start from the root element
$songs   = $xpath->query( "//div[@class = 'row content']//div[@class  = 'song']");
$artists = $xpath->query("//div[@class  = 'row content']//div[@class  = 'song']/following-sibling::*[1]");
$dates   = $xpath->query( "//div[@class = 'row content']//span[@class = 'timestamp']/@data-date");
$_date   = $dates->item(0);
$date    = substr($_date->nodeValue, 0, 10);

$radio_station = "WERS";

// DEBUGGING
// print "\nsongs         = " . var_export($songs, true) . "\n";
// print "\ndates         = " . var_export($dates, true) . "\n";
// print '\nsongs->length = ' . var_export($songs->length, true) . "\n";
// print '\ndates->length = ' . var_export($dates->length, true) . "\n";
// foreach ($dates as $domAttr)
//   print "\n100 d = " . $domAttr->value;

$rows_inserted = 0;

$radio_station_ids = array($radio_station => null);
$track_ids         = array();
$artist_ids        = array();
$origin_url_ids    = array($origin_url => null);

// Build our hashes of tracks,artists,stations
// from the XML parse
for ($i = 0; $i < $songs->length; $i++) {

  // song (aka track) and artist
  $song_item   = $songs->item($i);
  $artist_item = $artists->item($i);
  $song        = trim($song_item->nodeValue);
  $artist      = trim($artist_item->nodeValue);

  // timestamp
  //
  // FIXME: Hack to workaround the fact that we end up parsing
  // twice as many tracks as timestamps
  $_time = $dates->item(floor($i/2));
  $time = $_time->nodeValue;
  $time = str_replace("T", " ", $time, $c=1);


  // Store the ids for later lookup and caching
  $track_ids[$song]           = null;
  $artist_ids[$artist]         = null;
  $origin_url_ids[$origin_url] = null;

  $spin_tuples[$i] = array($song, $artist, $time);

  // DEBUGGING
  // print "\nsong = " . var_export($song, true) . "\n";
  // print "\nartist = " . var_export($artist, true) . "\n";
  // print "\n130 date = " .   var_export($date, true) . "\n";
  // print "\n130 time = " .   var_export($time, true) . "\n";
}

// DEBUGGING
// print "130" . var_dump($spin_tuples);

$artist_ids        = cache_database_ids($artist_ids        , "artists");
$track_ids         = cache_database_ids($track_ids         , "tracks");
$radio_station_ids = cache_database_ids($radio_station_ids , "radio_stations");
$origin_url_ids    = cache_database_ids($origin_url_ids    , "origin_url");

// DEBUGGING
// $track_ids = cache_database_ids($track_ids         , "tracks");
// print "\n139 track_ids = " .   var_export($track_ids, true) . "\n";

$radio_station_id = $radio_station_ids[$radio_station];
$origin_url_id    = $origin_url_ids[$origin_url];

for ($i = 0; $i < count($spin_tuples); $i++) {

  $track     = $spin_tuples[$i][0];
  $artist    = $spin_tuples[$i][1];
  $time      = $spin_tuples[$i][2];

  // DEBUGGING
  // print "\n147 track  = " . $track;
  // print "\n147 artist = " . $artist;
  // print "\n147 time   = " . $time;

  $track_id  = $track_ids[$track];
  $artist_id = $artist_ids[$artist];

  $sql = "INSERT INTO spins " .
      " (track_id,
         artist_id,
         time_spun,
         radio_station_id,
         origin_url_id) 
      VALUES 
        ('$track_id',
         '$artist_id',
         '$time',
         '$radio_station_id',
         '$origin_url_id' )";

  if (! mysqli_query($con, $sql)) {
    error_log("This query failed: $sql");
    error_log("mysqli_errno: (" . mysqli_errno($con) . ") ");
    error_log("mysqli_error: (" . mysqli_error($con) . ") ");
  } else {
    $rows_inserted++;
  }
}

print("\n$rows_inserted rows inserted\n");

mysqli_close($con);

?>

<?php

// Helper function to polymorphize a DOMNodeList as an array
function DOMNodeListToArray($DOMNodeList) {
  $ret = array();

  if (is_object($DOMNodeList) and "DOMNodeList" == get_class($DOMNodeList)) {
    for ($i = 0; $i < $DOMNodeList->length; $i++) {
      $n = $DOMNodeList->item($i);
      array_push($ret, $n->nodeValue);
    }
  } else if (is_array($DOMNodeList)) {
    $ret = $DOMNodeList;
  } else {
    error_log("This isn't an array or DOMNodeList: $DOMNodeList\n");
  }
  return $ret;

}

/*
 * Parse (err, scrape?) a playlist from an HTML page 
 *
 * Return the list of radio plays (aka spins) as a list of tuples.
 * But since this is PHP and not Python, it's calleda "multidimensional array".
 */

function parse_playlist(
 $origin_url,
 $song_xpath_query,
 $artist_xpath_query,
 $date_xpath_query,
 $radio_station_xpath_query,
 $time_regex,
 $radio_station_regex) {
  
  // Load our HTML file, which we pretend is an XML file
  $doc = new DOMDocument('1.0', 'utf-8');
  $doc->strictErrorChecking = FALSE;
  $doc = DOMDocument::loadHTMLFile($origin_url);

  $xpath = new DOMXPath($doc);

  // FIXME: get the date using XPATH
  $date = date("Y-m-d");

  // We start from the root element
  $songs_DOMNodeList   = $xpath->query($song_xpath_query);
  $artists_DOMNodeList = $xpath->query($artist_xpath_query);
  $dates_DOMNodeList   = $xpath->query($date_xpath_query);
  $stations_DOMNodeList = $xpath->query($radio_station_xpath_query);

  // DEBUGGING
  // var_dump(get_defined_vars());

  // Fill the array with radio stations if the length doesn't match
  // the number of tracks (e.g., if we're scraping a playlist
  // from only one radio station
  if ($stations_DOMNodeList->length < $songs_DOMNodeList->length) {
    $station_name_node = $stations_DOMNodeList->item(0);
    $station_name = $station_name_node->nodeValue;
    $stations_DOMNodeList = array();
    $stations_DOMNodeList = array_fill(0, $songs_DOMNodeList->length, $station_name);
  }

  // Convert our DOMNodeList to an array.  It's either an actual node list
  // from our parsed XML, or an array_fill'd array of the identical station name
  $stations_array = DOMNodeListToArray($stations_DOMNodeList);

  // Build our list of spin tuples: track,artist,timestamp,stations
  for ($i = 0; $i < $songs_DOMNodeList->length; $i++) {

    // song (aka track) and artist
    $song_item   = $songs_DOMNodeList->item($i);
    $artist_item = $artists_DOMNodeList->item($i);

    $song        = trim($song_item->nodeValue);
    $artist      = trim($artist_item->nodeValue);

    $radio_station = trim($stations_array[$i]);

    // timestamp
    // FIXME: Hack to workaround the fact that we end up parsing
    // twice as many tracks as timestamps
    $_time = $dates_DOMNodeList->item(floor($i/2));
    $time = trim($_time->nodeValue);

    // We may need to capture a piece of the DOMNode's value with a regex
    if (! is_null($radio_station_regex)) {
      preg_match("/$radio_station_regex/", $radio_station, $matches);
      $radio_station = $matches[1];
    }
    if (! is_null($time_regex)) {
      preg_match("/$time_regex/", $time, $matches);
      $time = $matches[1];
    }

    $datetime = $date . " " . $time;

    $spin_tuples[$i] = array($song, $artist, $datetime, $radio_station);
  }

  // DEBUGGING
  // print "\n spin_tuples = " . var_export($spin_tuples, true) . "\n";

  return $spin_tuples;
}

?>
